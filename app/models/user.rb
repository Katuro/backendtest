class User < ApplicationRecord
  validates :pseudonyme, presence: true, uniqueness: { case_sensitive: false }, format: { with: /[A-Z]{3}+/, message: 'Pseudonyme must be only 3 uppercase letters.'}

  def self.create_with_pseudonyme
    User.create!(pseudonyme: create_available_pseudonyme)
  end

  # Generate an Array from AAA to ZZZ, remove from it the pseudonymes
  # that are already taken then choose the first.
  def self.create_available_pseudonyme
    s = 'AAA'
    array = []

    while s != 'ZZZ'
      array << s
      s = s.next
    end
    array << 'ZZZ'
    array -= User.pluck(:pseudonyme)

    return array.first
  end

end

