class Api::V1::UsersController < ApplicationController

  # GET /users
  def index
    @users = User.all
    render json: @users
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: { message: 'OK.' }, status: 200
    else
      @user = User.create_with_pseudonyme
      render json: @user.pseudonyme
    end
  end

  private

  def user_params
    params.require(:user).permit(:pseudonyme)
  end
end
